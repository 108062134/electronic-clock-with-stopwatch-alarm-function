`timescale 1ps/1ps

module eletronic_clock(
    input clk,
    input lap,
    input stop,
    input accelerate,
    input showing_time,
    input left,
    input right,
    output  [3:0] sel,
    output  [6:0] Display,
    output reg [5:0] led,
    output reg [2:0] state = 3'd5,
    output reg [1:0] display = 2'b01,
    output reg [2:0] show_state
); 
wire clk_1HZ,clk_60HZ;
wire clk_accelerate;
reg clk_new;
reg [3:0] min_tens,min_digits;
reg [3:0] sec_tens,sec_digits;
reg [3:0] min_tens_tmp,min_digits_tmp;
reg [3:0] sec_tens_tmp,sec_digits_tmp,num;
reg [3:0] hour_tens,hour_tens_tmp;
reg [3:0] hour_digits,hour_digits_tmp;
reg [3:0] day_digits,day_digits_tmp = 4'd1;
reg [3:0] day_tens,day_tens_tmp = 4'd0;
reg [3:0] month_tens,month_tens_tmp = 4'd0;
reg [3:0] month_digits,month_digits_tmp = 4'd1;
wire [3:0] max_day_a_month_ten;
wire [3:0] max_day_a_month_digit;
reg [3:0] year_tens,year_tens_tmp = 4'd0;
reg [3:0] year_digits,year_digits_tmp = 4'd1;
reg [3:0] a,b,c,d,e,f,g,h,i,j,k,l;

reg [3:0] alarm_min_tens,alarm_min_digits;
reg [3:0] alarm_sec_tens,alarm_sec_digits;
reg [3:0] alarm_hour_tens,alarm_hour_digits;
reg [3:0] alarm_day_tens,alarm_day_digits;

reg [3:0] alarm_min_tens_tmp,alarm_min_digits_tmp;
reg [3:0] alarm_sec_tens_tmp,alarm_sec_digits_tmp;
reg [3:0] alarm_hour_tens_tmp,alarm_hour_digits_tmp;
reg [3:0] alarm_day_tens_tmp,alarm_day_digits_tmp;

wire [2:0] show_state_tmp;

reg [2:0] nxt_state = 3'd5;
reg one_min = 1'b0;
reg one_hour = 1'b0;
reg one_day = 1'b0;
reg one_month = 1'b0;
reg one_year = 1'b0;
reg enable = 1'd0;

wire de_lap,one_lap,de_stop,one_stop;
wire de_left,de_right;
wire de_showing_time,one_showing_time;

wire short_push_left,short_push_right;
wire long_push_left,long_push_right;

wire div_clk_100hz,div_clk_800hz;
reg [3:0] st_sec_digits,st_min_tens,st_min_digits,st_sec_tens;
reg [3:0] st_hour_tens,st_hour_digits,st_day_tens,st_day_digits;
reg [3:0] st_month_tens,st_month_digits,st_year_tens,st_year_digits;

reg setting_time_flag ;
reg setting_alarm_flag ;
reg twinkle ;



// one sec clk
counter C(
    clk,
    clk_1HZ
);
freq_div_800hz f_800hz(
    clk,
    clk_accelerate
);

freq_div_100hz f_100(
    clk,
    div_clk_100hz
);

debounce d_lap(
    div_clk_100hz,
    lap,
    de_lap
);
debounce d_stop(
    div_clk_100hz,
    stop,
    de_stop
);
debounce d_left(
    div_clk_100hz,
    left,
    de_left
);
debounce d_right(
    div_clk_100hz,
    right,
    de_right
);
debounce d_showing_time(
    div_clk_100hz,
    showing_time,
    de_showing_time
);

onepulse o_lap(
    clk_new,
    de_lap,
    one_lap
);
onepulse o_stop(
    clk_new,
    de_stop,
    one_stop
);
onepulse o_showing_time(
    clk_new,
    de_showing_time,
    one_showing_time
);


freq_div_60HZ f_d(
    clk,
    clk_60HZ
);

DFF_4bits D(
    clk_60HZ,
    sel
);
seven_segment s(
    num,
    Display
);

long_or_short_push ll_l(
    de_left,
    clk_new,
    short_push_left,
    long_push_left
);

long_or_short_push ll_r(
    de_right,
    clk_new,
    short_push_right,
    long_push_right
);


days_a_month Days(
    month_tens,
    month_digits,
    max_day_a_month_ten,
    max_day_a_month_digit
);

show_state_control SC(
    clk_new,
    one_showing_time,
    show_state_tmp
);

always@(posedge clk_new)begin
    if(state == 3'd5)begin    // initial state
        st_min_tens <= 4'd0;
        st_sec_digits <= 4'd0;
        st_sec_tens <= 4'd0;
        st_min_digits <= 4'd0;
        st_hour_digits <= 4'd0;
        st_hour_tens <= 4'd0;
        st_day_digits <= 4'd0;
        st_day_tens <= 4'd0;
        st_month_digits <= 4'd0;
        st_month_tens <= 4'd0 ;
        st_year_digits <= 4'd0 ;
        st_year_tens <= 4'd0;
        
        min_digits <= 4'd0;
        min_tens <= 4'd0;
        sec_digits <= 4'd0;
        sec_tens <= 4'd0;
        hour_digits <= 4'd0;
        hour_tens <= 4'd0;
        day_digits <= 4'd1;
        day_tens <= 4'd0;
        month_digits <= 4'd1;
        month_tens <= 4'd0;
        year_digits <= 4'd0;
        year_tens <= 4'd0;

        alarm_sec_digits <= 4'd0;
        alarm_sec_tens <= 4'd0;
        alarm_min_digits <= 4'd0;
        alarm_min_tens <= 4'd0;
        alarm_hour_digits <= 4'd0;
        alarm_hour_tens <= 4'd0;
        alarm_day_digits <= 4'd0;
        alarm_day_tens <= 4'd0;

        state <= nxt_state;
        show_state <= show_state_tmp;
    end
    else if(state == 3'd3)begin       // lap state
        st_min_tens <= 4'd0 + st_min_tens;
        st_sec_digits <= 4'd0 + st_sec_digits;
        st_sec_tens <= 4'd0 + st_sec_tens;
        st_min_digits <= 4'd0 + st_min_digits;
        st_hour_digits <= 4'd0 + st_hour_digits;
        st_hour_tens <= 4'd0 + st_hour_tens;
        st_day_digits <= 4'd0 + st_day_digits;
        st_day_tens <= 4'd0 + st_day_tens;
        st_month_digits <= 4'd0 + st_month_digits;
        st_month_tens <= 4'd0 + st_month_tens;
        st_year_digits <= 4'd0 + st_year_digits;
        st_year_tens <= 4'd0 + st_year_tens;

        min_digits <= min_digits_tmp;
        min_tens <= min_tens_tmp;
        sec_digits <= sec_digits_tmp;
        sec_tens <= sec_tens_tmp;
        hour_digits <= hour_digits_tmp;
        hour_tens <= hour_tens_tmp;
        day_digits <= day_digits_tmp;
        day_tens <= day_tens_tmp;
        month_digits <= month_digits_tmp;
        month_tens <= month_tens_tmp;
        year_digits <= year_digits_tmp;
        year_tens <= year_tens_tmp;

        alarm_sec_digits <= alarm_sec_digits_tmp;
        alarm_sec_tens <= alarm_sec_tens_tmp;
        alarm_min_digits <= alarm_min_digits_tmp;
        alarm_min_tens <= alarm_min_tens_tmp;
        alarm_hour_digits <= alarm_hour_digits_tmp;
        alarm_hour_tens <= alarm_hour_tens_tmp;
        alarm_day_digits <= alarm_day_digits_tmp;
        alarm_day_tens <= alarm_day_tens_tmp;

        state <= nxt_state;
        show_state <= show_state_tmp;
    end
    else if(state == 3'd4)begin         // transition state
        st_min_tens <= 4'd0 + st_min_tens;
        st_sec_digits <= 4'd0 + st_sec_digits;
        st_sec_tens <= 4'd0 + st_sec_tens;
        st_min_digits <= 4'd0 + st_min_digits;
        st_hour_digits <= 4'd0 + st_hour_digits;
        st_hour_tens <= 4'd0 + st_hour_tens;
        st_day_digits <= 4'd0 + st_day_digits;
        st_day_tens <= 4'd0 + st_day_tens;
        st_month_digits <= 4'd0 + st_month_digits;
        st_month_tens <= 4'd0 + st_month_tens;
        st_year_digits <= 4'd0 + st_year_digits;
        st_year_tens <= 4'd0 + st_year_tens;

        min_digits <= min_digits_tmp;
        min_tens <= min_tens_tmp;
        sec_digits <= sec_digits_tmp;
        sec_tens <= sec_tens_tmp;
        hour_digits <= hour_digits_tmp;
        hour_tens <= hour_tens_tmp;
        day_digits <= day_digits_tmp;
        day_tens <= day_tens_tmp;
        month_digits <= month_digits_tmp;
        month_tens <= month_tens_tmp;
        year_digits <= year_digits_tmp;
        year_tens <= year_tens_tmp;

        alarm_sec_digits <= alarm_sec_digits_tmp;
        alarm_sec_tens <= alarm_sec_tens_tmp;
        alarm_min_digits <= alarm_min_digits_tmp;
        alarm_min_tens <= alarm_min_tens_tmp;
        alarm_hour_digits <= alarm_hour_digits_tmp;
        alarm_hour_tens <= alarm_hour_tens_tmp;
        alarm_day_digits <= alarm_day_digits_tmp;
        alarm_day_tens <= alarm_day_tens_tmp;

        state <= nxt_state;
        show_state <= show_state_tmp;
    end
    else if(state == 3'd2 && one_lap == 1'b1)begin  // in stop state and click lap to reset 
        st_min_tens <= 4'd0;
        st_sec_digits <= 4'd0;
        st_sec_tens <= 4'd0;
        st_min_digits <= 4'd0;
        st_hour_digits <= 4'd0;
        st_hour_tens <= 4'd0;
        st_day_digits <= 4'd0;
        st_day_tens <= 4'd0;
        st_month_digits <= 4'd0;
        st_month_tens <= 4'd0 ;
        st_year_digits <= 4'd0 ;
        st_year_tens <= 4'd0;

        min_tens <= 4'd0;
        sec_digits <= 4'd0;
        sec_tens <= 4'd0;
        min_digits <= 4'd0;
        hour_digits <= 4'd0;
        hour_tens <= 4'd0;
        day_digits <= 4'd1;
        day_tens <= 4'd0;
        month_digits <=4'd1;
        month_tens <= 4'd0;
        year_digits <= 4'd0;
        year_tens <= 4'd0;

        alarm_sec_digits <= alarm_sec_digits_tmp;
        alarm_sec_tens <= alarm_sec_tens_tmp;
        alarm_min_digits <= alarm_min_digits_tmp;
        alarm_min_tens <= alarm_min_tens_tmp;
        alarm_hour_digits <= alarm_hour_digits_tmp;
        alarm_hour_tens <= alarm_hour_tens_tmp;
        alarm_day_digits <= alarm_day_digits_tmp;
        alarm_day_tens <= alarm_day_tens_tmp;

        state <= nxt_state;
        show_state <= show_state_tmp;
    end
    else if(nxt_state == 3'd4)begin   // the sec entering transition state
        st_min_tens <= min_tens;
        st_sec_digits <= sec_digits;
        st_sec_tens <= sec_tens;
        st_min_digits <= min_digits;
        st_hour_digits <= hour_digits;
        st_hour_tens <= hour_tens;
        st_day_digits <= day_digits;
        st_day_tens <= day_tens;
        st_month_digits <= month_digits;
        st_month_tens <= month_tens;
        st_year_digits <= year_digits;
        st_year_tens <= year_tens;
        
        min_digits <= min_digits_tmp;
        min_tens <= min_tens_tmp;
        sec_digits <= sec_digits_tmp;
        sec_tens <= sec_tens_tmp;
        hour_digits <= hour_digits_tmp;
        hour_tens <= hour_tens_tmp;
        day_digits <= day_digits_tmp;
        day_tens <= day_tens_tmp;
        month_digits <= month_digits_tmp;
        month_tens <= month_tens_tmp;
        year_digits <= year_digits_tmp;
        year_tens <= year_tens_tmp;

        alarm_sec_digits <= alarm_sec_digits_tmp;
        alarm_sec_tens <= alarm_sec_tens_tmp;
        alarm_min_digits <= alarm_min_digits_tmp;
        alarm_min_tens <= alarm_min_tens_tmp;
        alarm_hour_digits <= alarm_hour_digits_tmp;
        alarm_hour_tens <= alarm_hour_tens_tmp;
        alarm_day_digits <= alarm_day_digits_tmp;
        alarm_day_tens <= alarm_day_tens_tmp;

        state <= nxt_state;
        show_state <= show_state_tmp;
    end
    else begin
        st_min_tens <= 4'd0;
        st_sec_digits <= 4'd0;
        st_sec_tens <= 4'd0;
        st_min_digits <= 4'd0;
        st_hour_digits <= 4'd0;
        st_hour_tens <= 4'd0;
        st_day_digits <= 4'd0;
        st_day_tens <= 4'd0;
        st_month_digits <= 4'd0;
        st_month_tens <= 4'd0 ;
        st_year_digits <= 4'd0 ;
        st_year_tens <= 4'd0;
        
        min_digits <= min_digits_tmp;
        min_tens <= min_tens_tmp;
        sec_digits <= sec_digits_tmp;
        sec_tens <= sec_tens_tmp;
        hour_digits <= hour_digits_tmp;
        hour_tens <= hour_tens_tmp;
        day_digits <= day_digits_tmp;
        day_tens <= day_tens_tmp;
        month_digits <= month_digits_tmp;
        month_tens <= month_tens_tmp;
        year_digits <= year_digits_tmp;
        year_tens <= year_tens_tmp;

        alarm_sec_digits <= alarm_sec_digits_tmp;
        alarm_sec_tens <= alarm_sec_tens_tmp;
        alarm_min_digits <= alarm_min_digits_tmp;
        alarm_min_tens <= alarm_min_tens_tmp;
        alarm_hour_digits <= alarm_hour_digits_tmp;
        alarm_hour_tens <= alarm_hour_tens_tmp;
        alarm_day_digits <= alarm_day_digits_tmp;
        alarm_day_tens <= alarm_day_tens_tmp;

        state <= nxt_state;
        show_state <= show_state_tmp;
    end
    
end

// Debug mode with accelerated clock
always@(*)begin
    if(accelerate == 1'b1)begin
        clk_new = clk_accelerate;
    end
    else begin
        clk_new = clk_1HZ;
    end
end
// for year part
always@(*)begin
    if( (short_push_left == 1'b1 && show_state == 3'd4) && setting_time_flag == 1'b1 )begin
        if(year_digits == 4'd9 && year_tens == 4'd9)begin
            year_tens_tmp = 4'd0;
            year_digits_tmp = 4'd0;
        end
        else begin
            if(year_digits == 4'd9)begin
                year_tens_tmp = 4'd1 + year_tens;
                year_digits_tmp = 4'd0;
            end
            else begin
                year_tens_tmp = year_tens;
                year_digits_tmp = 4'd1 + year_digits;
            end
        end
    end
    else if(enable == 1'b1)begin
        if(one_year == 1'b1)begin
            if(year_digits == 4'd9 && year_tens == 4'd9)begin
                year_tens_tmp = 4'd0;
                year_digits_tmp = 4'd0;
            end
            else begin
                if(year_digits == 4'd9)begin
                    year_tens_tmp = 4'd1 + year_tens;
                    year_digits_tmp = 4'd0;
                end
                else begin
                    year_tens_tmp = year_tens;
                    year_digits_tmp = 4'd1 + year_digits;
                end
            end
        end
        else begin
            year_tens_tmp = year_tens + 4'd0;
            year_digits_tmp = year_digits + 4'd0;
        end
    end
    else begin
        year_tens_tmp = year_tens + 4'd0;
        year_digits_tmp = year_digits + 4'd0;
    end
end
// for month part
always@(*)begin
    if( ((short_push_left == 1'b1 && show_state == 3'd3)||(short_push_right == 1'b1 && show_state == 3'd4)) && setting_time_flag == 1'b1 )begin
        one_year = one_year + 1'b0;
        if(month_digits == 4'd2 && month_tens == 4'd1)begin
            month_tens_tmp = 4'd0;
            month_digits_tmp = 4'd1;
        end
        else begin
            if(month_digits == 4'd9)begin
                month_tens_tmp = 4'd1 + month_tens;
                month_digits_tmp = 4'd0;
            end
            else begin
                month_tens_tmp = month_tens;
                month_digits_tmp = 4'd1 + month_digits;
            end
        end
    end
    else if(enable == 1'b1)begin
        if(one_month == 1'b1)begin
            if(month_digits == 4'd2 && month_tens == 4'd1)begin
                month_tens_tmp = 4'd0;
                month_digits_tmp = 4'd1;
                one_year = 1'b1;
            end
            else begin
                if(month_digits == 4'd9)begin
                    month_tens_tmp = 4'd1 + month_tens;
                    month_digits_tmp = 4'd0;
                    one_year = 1'b0;
                end
                else begin
                    month_tens_tmp = month_tens;
                    month_digits_tmp = 4'd1 + month_digits;
                    one_year = 1'b0;
                end
            end
        end
        else begin
            month_tens_tmp = month_tens + 4'd0;
            month_digits_tmp = month_digits + 4'd0;
            one_year = 1'b0;
        end
    end
    else begin
        month_tens_tmp = month_tens + 4'd0;
        month_digits_tmp = month_digits + 4'd0;
        one_year = 1'b0;
    end
end
// for day part
always@(*)begin
    if( ((short_push_left == 1'b1 && show_state == 3'd2)||(short_push_right == 1'b1 && show_state == 3'd3)) && setting_alarm_flag == 1'b1 )begin
        one_month = one_month + 1'b0;
        if(alarm_day_tens == max_day_a_month_ten && alarm_day_digits == max_day_a_month_digit )begin
            alarm_day_tens_tmp = 4'd0;
            alarm_day_digits_tmp = 4'd1;
        end
        else if(day_digits == 4'd9)begin
            alarm_day_tens_tmp = alarm_day_tens + 4'd1;
            alarm_day_digits_tmp = 4'd0;
        end
        else begin
            alarm_day_tens_tmp = alarm_day_tens + 4'd0;
            alarm_day_digits_tmp = alarm_day_digits + 4'd1;
        end
        day_tens_tmp = day_tens + 4'd0;
        day_digits_tmp = day_digits + 4'd0;
    end
    else if( ((short_push_left == 1'b1 && show_state == 3'd2)||(short_push_right == 1'b1 && show_state == 3'd3)) && setting_time_flag == 1'b1 )begin
        one_month = one_month + 1'b0;
        if(day_tens == max_day_a_month_ten && day_digits == max_day_a_month_digit )begin
            day_tens_tmp = 4'd0;
            day_digits_tmp = 4'd1;
        end
        else if(day_digits == 4'd9)begin
            day_tens_tmp = day_tens + 4'd1;
            day_digits_tmp = 4'd0;
        end
        else begin
            day_tens_tmp = day_tens + 4'd0;
            day_digits_tmp = day_digits + 4'd1;
        end
        alarm_day_tens_tmp = alarm_day_tens + 4'd0;
        alarm_day_digits_tmp = alarm_day_digits + 4'd0;
    end
    else if(enable == 1'b1)begin
        if(one_day == 1'b1)begin
            if(day_tens == max_day_a_month_ten && day_digits == max_day_a_month_digit )begin
                day_tens_tmp = 4'd0;
                day_digits_tmp = 4'd0;
                one_month = 1'b1;
            end
            else if(day_digits == 4'd9)begin
                day_tens_tmp = day_tens + 4'd1;
                day_digits_tmp = 4'd0;
                one_month = 1'b0;
            end
            else begin
                day_tens_tmp = day_tens + 4'd0;
                day_digits_tmp = day_digits + 4'd1;
                one_month = 1'b0;
            end
        end
        else begin
            day_tens_tmp = day_tens + 4'd0;
            day_digits_tmp = day_digits + 4'd0;
            one_month = 1'b0;
        end
        alarm_day_tens_tmp = alarm_day_tens + 4'd0;
        alarm_day_digits_tmp = alarm_day_digits + 4'd0;
    end
    else begin
        day_tens_tmp = day_tens + 4'd0;
        day_digits_tmp = day_digits + 4'd0;
        alarm_day_tens_tmp = alarm_day_tens + 4'd0;
        alarm_day_digits_tmp = alarm_day_digits + 4'd0;
        one_month = 1'b0;
    end
end
// for hour part
always@(*)begin
    if( ((short_push_left == 1'b1 && show_state == 3'd1)||(short_push_right == 1'b1 && show_state == 3'd2)) && setting_alarm_flag == 1'b1 )begin
        one_day = one_day + 1'b0;
        if(alarm_hour_tens == 4'd2 && alarm_hour_digits == 4'd3)begin
            alarm_hour_digits_tmp = 4'd0;
            alarm_hour_tens_tmp = 4'd0;
        end
        else begin
            if(alarm_hour_digits == 4'd9)begin
                alarm_hour_digits_tmp = 4'd0;
                alarm_hour_tens_tmp = alarm_hour_tens + 4'd1;
            end
            else begin
                alarm_hour_digits_tmp = alarm_hour_digits + 4'd1;
                alarm_hour_tens_tmp = alarm_hour_tens + 4'd0;
            end
        end
        hour_digits_tmp = hour_digits;
        hour_tens_tmp = hour_tens;
    end
    else if( ((short_push_left == 1'b1 && show_state == 3'd1)||(short_push_right == 1'b1 && show_state == 3'd2)) && setting_time_flag == 1'b1 )begin
        one_day = one_day + 1'b0;
        if(hour_tens == 4'd2 && hour_digits == 4'd3)begin
            hour_digits_tmp = 4'd0;
            hour_tens_tmp = 4'd0;
        end
        else begin
            if(hour_digits == 4'd9)begin
                hour_digits_tmp = 4'd0;
                hour_tens_tmp = hour_tens + 4'd1;
            end
            else begin
                hour_digits_tmp = hour_digits + 4'd1;
                hour_tens_tmp = hour_tens + 4'd0;
            end
        end
        alarm_hour_digits_tmp = alarm_hour_digits + 4'd0;
        alarm_hour_tens_tmp = alarm_hour_tens + 4'd0;
    end
    else if(enable == 1'b1)begin
        if(one_hour == 1'b1)begin
            if(hour_tens == 4'd2 && hour_digits == 4'd3)begin
                one_day = 1'b1;
                hour_digits_tmp = 4'd0;
                hour_tens_tmp = 4'd0;
            end
            else begin
                one_day = 1'b0;
                if(hour_digits == 4'd9)begin
                    hour_digits_tmp = 4'd0;
                    hour_tens_tmp = hour_tens + 4'd1;
                end
                else begin
                    hour_digits_tmp = hour_digits + 4'd1;
                    hour_tens_tmp = hour_tens + 4'd0;
                end
            end
        end
        else begin
            one_day = 1'b0;
            hour_digits_tmp = hour_digits;
            hour_tens_tmp = hour_tens;
        end
        alarm_hour_digits_tmp = alarm_hour_digits + 4'd0;
        alarm_hour_tens_tmp = alarm_hour_tens + 4'd0;
    end
    else begin
        one_day = one_day + 1'b0;
        hour_digits_tmp = hour_digits;
        hour_tens_tmp = hour_tens;
        alarm_hour_digits_tmp = alarm_hour_digits + 4'd0;
        alarm_hour_tens_tmp = alarm_hour_tens + 4'd0;
    end
end
// for min part
always@(*)begin
    if(( (short_push_left == 1'b1 && show_state == 3'd0) ||(short_push_right == 1'b1 && show_state == 3'd1) ) && setting_alarm_flag == 1'b1 )begin
        one_hour = one_hour + 1'b0;
        if(alarm_min_tens == 4'd5 && alarm_min_digits == 4'd9)begin
            alarm_min_tens_tmp = 4'd0;
            alarm_min_digits_tmp = 4'd0;
        end
        else begin
            if(alarm_min_digits == 4'd9)begin
                alarm_min_digits_tmp = 4'd0;
                alarm_min_tens_tmp = alarm_min_tens + 4'd1;
            end
            else begin
                alarm_min_digits_tmp = alarm_min_digits + 4'd1;
                alarm_min_tens_tmp = alarm_min_tens + 4'd0;
            end
        end
        min_tens_tmp = min_tens + 4'd0;
        min_digits_tmp = min_digits + 4'd0; 
    end
    else if( ( (short_push_left == 1'b1 && show_state == 3'd0) ||(short_push_right == 1'b1 && show_state == 3'd1) ) && setting_time_flag == 1'b1)begin
        one_hour = one_hour + 1'b0;
        if(min_tens == 4'd5 && min_digits == 4'd9)begin
            min_tens_tmp = 4'd0;
            min_digits_tmp = 4'd0;
        end
        else begin
            if(min_digits == 4'd9)begin
                min_digits_tmp = 4'd0;
                min_tens_tmp = min_tens + 4'd1;
            end
            else begin
                min_digits_tmp = min_digits + 4'd1;
                min_tens_tmp = min_tens + 4'd0;
            end
        end
        alarm_min_tens_tmp = alarm_min_tens + 4'd0;
        alarm_min_digits_tmp = alarm_min_digits + 4'd0; 
    end
    else if(enable == 1'b1)begin
        if(one_min == 1'b1)begin
            if(min_tens == 4'd5 && min_digits == 4'd9)begin
                one_hour = 1'b1;
                min_tens_tmp = 4'd0;
                min_digits_tmp = 4'd0;
            end
            else begin
                one_hour = 1'b0;
                if(min_digits == 4'd9)begin
                    min_digits_tmp = 4'd0;
                    min_tens_tmp = min_tens + 4'd1;
                end
                else begin
                    min_digits_tmp = min_digits + 4'd1;
                    min_tens_tmp = min_tens + 4'd0;
                end
            end
        end
        else begin
            one_hour = 1'b0;
            min_tens_tmp = min_tens + 4'd0;
            min_digits_tmp = min_digits + 4'd0;
        end
        alarm_min_tens_tmp = alarm_min_tens + 4'd0;
        alarm_min_digits_tmp = alarm_min_digits + 4'd0; 
    end
    else begin
        min_tens_tmp = min_tens + 4'd0;
        min_digits_tmp = min_digits + 4'd0;
        alarm_min_tens_tmp = alarm_min_tens + 4'd0;
        alarm_min_digits_tmp = alarm_min_digits + 4'd0; 
        one_hour = one_hour + 1'b0;
    end
end
// for sec part
always@(*)begin
    if( (setting_alarm_flag == 1'b1 && show_state == 3'd0)&& short_push_right == 1'b1 )begin
        one_min = one_min + 1'b0;
        if(alarm_sec_tens == 4'd5 && alarm_sec_digits == 4'd9)begin
            alarm_sec_tens_tmp = 4'd0;
            alarm_sec_digits_tmp = 4'd0;
        end
        else begin 
            if(alarm_sec_digits == 4'd9)begin
                alarm_sec_tens_tmp = alarm_sec_tens + 4'd1;
                alarm_sec_digits_tmp = 4'd0;
            end
            else begin
                alarm_sec_tens_tmp = alarm_sec_tens + 4'd0;
                alarm_sec_digits_tmp = alarm_sec_digits + 4'd1;
            end
        end
        sec_tens_tmp = sec_tens + 4'd0;
        sec_digits_tmp = sec_digits + 4'd0;
    end
    else if( (setting_time_flag == 1'b1 && show_state == 3'd0)&& short_push_right == 1'b1)begin
        one_min = one_min + 1'b0;
        if(sec_tens == 4'd5 && sec_digits == 4'd9)begin
            sec_tens_tmp = 4'd0;
            sec_digits_tmp = 4'd0;
        end
        else begin 
            if(sec_digits == 4'd9)begin
                sec_tens_tmp = sec_tens + 4'd1;
                sec_digits_tmp = 4'd0;
            end
            else begin
                sec_tens_tmp = sec_tens + 4'd0;
                sec_digits_tmp = sec_digits + 4'd1;
            end
        end
        alarm_sec_tens_tmp = alarm_sec_tens + 4'd0;
        alarm_sec_digits_tmp = alarm_sec_digits + 4'd0;
    end
    else if(enable == 1'b1)begin
        if(sec_tens == 4'd5 && sec_digits == 4'd9)begin
            sec_tens_tmp = 4'd0;
            sec_digits_tmp = 4'd0;
            one_min = 1'd1;
        end
        else begin 
            one_min = 1'd0;
            if(sec_digits == 4'd9)begin
                sec_tens_tmp = sec_tens + 4'd1;
                sec_digits_tmp = 4'd0;
            end
            else begin
                sec_tens_tmp = sec_tens + 4'd0;
                sec_digits_tmp = sec_digits + 4'd1;
            end
        end
        alarm_sec_tens_tmp = alarm_sec_tens + 4'd0;
        alarm_sec_digits_tmp = alarm_sec_digits + 4'd0;
    end
    else begin
        one_min = one_min + 1'b0;
        sec_tens_tmp = sec_tens + 4'd0;
        sec_digits_tmp = sec_digits + 4'd0;
        alarm_sec_tens_tmp = alarm_sec_tens + 4'd0;
        alarm_sec_digits_tmp = alarm_sec_digits + 4'd0;
    end
end
reg [5:0] led_tmp = 6'b111110;

// if the time match led should twinkle
reg [5:0] count , count_tmp ;
always@(posedge clk_1HZ)begin
    led_tmp <= {led_tmp[4:0],led_tmp[5]};
    count <= count_tmp;
end
always@(*)begin
    if(twinkle == 1'b1)begin
        led = led_tmp;
        count_tmp = count + 6'd1;
    end
    else begin
        led = 6'd0;
        count_tmp = 6'd0;
    end
end

// state transition
always@(*)begin
    case(state)
    // twinckle !
    3'd0:begin
        twinkle = 1'b1;
        enable = 1'b1;
        display = 2'b01;
        setting_time_flag = 1'b0;
        setting_alarm_flag = 1'b0;
        if(count == 6'd31)begin
            nxt_state = 3'd1;
        end
        else begin
            nxt_state = 3'd0;
        end
    end
    // counting state
    3'd1:begin
        if(alarm_sec_digits == sec_digits && alarm_sec_tens == sec_tens && alarm_min_tens == min_tens 
            && alarm_min_digits == min_digits && alarm_hour_digits == hour_digits && alarm_hour_tens == hour_tens 
            && alarm_day_digits == day_digits && alarm_day_tens == day_tens)begin
            nxt_state = 3'd0;
        end
        else if(one_lap == 1'b1)begin
            nxt_state = 3'd4;
        end
        else if(one_stop == 1'b1)begin
            nxt_state = 3'd2;
        end
        else begin
            nxt_state = state + 3'd0;
        end
        enable = 1'b1;
        display = 2'b01;
        setting_time_flag = 1'b0;
        setting_alarm_flag = 1'b0;
        twinkle = 1'b0;
    end
    // stop state
    3'd2:begin
        if(one_stop == 1'b1)begin
            nxt_state = 3'd1;
        end
        else if(one_lap == 1'b1)begin
            nxt_state = 3'd1;
        end
        else begin
            nxt_state = state + 3'd0;
        end
        enable = 1'b0;
        display = 2'b01;
        twinkle = 1'b0;
        setting_time_flag = 1'b0;
        setting_alarm_flag = 1'b0;
    end
    // lap state
    3'd3:begin
        if(one_lap == 1'b1)begin
            nxt_state = 3'd1;
        end
        else begin
            nxt_state = 3'd3;
        end
        enable = 1'b1;
        display = 2'b00;
        twinkle = 1'b0;  
        setting_time_flag = 1'b0;
        setting_alarm_flag = 1'b0;
    end
    // transition state for lap 
    3'd4:begin
        nxt_state = 3'd3;
        enable = 1'b1;
        display = 2'b00;
        twinkle = 1'b0;
        setting_time_flag = 1'b0;
        setting_alarm_flag = 1'b0;
    end
    // initial state
    3'd5:begin
        if(long_push_left == 1'b1)begin  // set initial time
            nxt_state = 3'd6;
        end
        else if(long_push_right == 1'b1)begin  // set alarm
            nxt_state = 3'd7;
        end
        else begin
            nxt_state = 3'd5;
        end
        enable = 1'b0;
        display = 2'b01;
        twinkle = 1'b0;
        setting_time_flag = 1'b0;
        setting_alarm_flag = 1'b0;
    end
    // setting initial time state
    3'd6:begin
        if(long_push_right == 1'b1)begin   // setting finish 
            nxt_state = 3'd1;
        end
        else if(long_push_left == 1'b1)begin
            nxt_state = 3'd7;
        end
        else begin
            nxt_state = 3'd6;
        end
        enable = 1'b0;
        display = 2'b01;
        twinkle = 1'b0;
        setting_time_flag = 1'b1;
        setting_alarm_flag = 1'b0;
    end
    // setting alarm state
    3'd7:begin
        if(long_push_right == 1'b1)begin
            nxt_state = 3'd1;
        end
        else begin  
            nxt_state = 3'd7;
        end
        enable = 1'b0;
        display = 2'b11;
        twinkle = 1'b0;
        setting_time_flag = 1'b0;
        setting_alarm_flag = 1'b1;
    end
    default:begin
        nxt_state = 3'd1;
        enable = 1'b0;
        display = 2'b01;
        twinkle = 1'b0;
        setting_time_flag = 1'b0;
        setting_alarm_flag = 1'b0;
    end
    endcase
end

// state transition for showing days,months,years...
always@(*)begin
    case(show_state)
    // min + sec
    3'd0:begin
        a = min_tens;
        b = min_digits;
        c = sec_tens;
        d = sec_digits;
        e = st_min_tens;
        f = st_min_digits;
        g = st_sec_tens;
        h = st_sec_digits;
        i = alarm_min_tens;
        j = alarm_min_digits;
        k = alarm_sec_tens;
        l = alarm_sec_digits;
    end
    // hour + min
    3'd1:begin
        a = hour_tens;
        b = hour_digits;
        c = min_tens;
        d = min_digits;
        e = st_hour_tens;
        f = st_hour_digits;
        g = st_min_tens;
        h = st_min_digits;
        i = alarm_hour_tens;
        j = alarm_hour_digits;
        k = alarm_min_tens;
        l = alarm_min_digits;
    end
    // day + hour
    3'd2:begin
        a = day_tens;
        b = day_digits;
        c = hour_tens;
        d = hour_digits;
        e = st_day_tens;
        f = st_day_digits;
        g = st_hour_tens;
        h = st_hour_digits;
        i = alarm_day_tens;
        j = alarm_day_digits;
        k = alarm_hour_tens;
        l = alarm_hour_digits;
    end
    // month + day
    3'd3:begin
        a = month_tens;
        b = month_digits;
        c = day_tens;
        d = day_digits;
        e = st_month_tens;
        f = st_month_digits;
        g = st_day_tens;
        h = st_day_digits;
        i = 4'd0;
        j = 4'd0;
        k = alarm_day_tens;
        l = alarm_day_digits;
    end
    // year + month
    3'd4:begin
        a = year_tens;
        b = year_digits;
        c = month_tens;
        d = month_digits;
        e = st_year_tens;
        f = st_year_digits;
        g = st_month_tens;
        h = st_month_digits;
        i = 4'd0;
        j = 4'd0;
        k = 4'd0;
        l = 4'd0;
    end
    default:begin
        a = min_tens;
        b = min_digits;
        c = sec_tens;
        d = sec_digits;
        e = st_min_tens;
        f = st_min_digits;
        g = st_sec_tens;
        h = st_sec_digits;
        i = alarm_min_tens;
        j = alarm_min_digits;
        k = alarm_sec_tens;
        l = alarm_sec_digits;
    end
    endcase
end

// original display + lap display 
always@(*)begin
    if(display == 2'b01)begin
        case(sel)
            4'b1110: num = d;
            4'b1101: num = c;
            4'b1011: num = b;
            4'b0111: num = a;
            default: num = d;
        endcase
    end
    else if(display == 2'b00)begin
        case(sel)
            4'b1110: num = h;
            4'b1101: num = g;
            4'b1011: num = f;
            4'b0111: num = e;
            default: num = h;
        endcase
    end
    else if(display == 2'b11)begin
        case(sel)
            4'b1110: num = l;
            4'b1101: num = k;
            4'b1011: num = j;
            4'b0111: num = i;
            default: num = l;
        endcase
    end
    else begin
        case(sel)
            4'b1110: num = d;
            4'b1101: num = c;
            4'b1011: num = b;
            4'b0111: num = a;
            default: num = d;
        endcase
    end
end

endmodule