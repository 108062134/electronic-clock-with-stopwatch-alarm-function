`timescale 1ps/1ps

module long_or_short_push(
    input signal,
    input clk,
    output reg short_push,
    output reg long_push
);

reg [5:0] cnt = 6'd0;
reg [5:0] cnt_tmp = 6'd0;
reg delay_siganl;

wire up_edge;
wire down_edge;


assign up_edge = delay_siganl & (~signal);
assign down_edge = signal & (~delay_siganl);

always@(posedge clk)begin
    delay_siganl <= signal;
end


always@(posedge clk)begin
    if(signal == 1'b1 && delay_siganl == 1'b1)begin
        cnt <= cnt_tmp;
    end
    else if(down_edge == 1'b1)begin
        cnt <= 6'd0;
    end
    else begin
        cnt <= 6'd0;
    end
end

always@(*)begin
    cnt_tmp = cnt + 6'd1;
end

always@(posedge clk)begin
    if(up_edge == 1'b1)begin
        if(cnt >= 6'd2)begin
            long_push <= 1'b1;
        end
        else begin
            short_push <= 1'b1;
        end
    end
    else begin
        long_push <= 1'b0;
        short_push <= 1'b0;
    end
end

endmodule