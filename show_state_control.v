`timescale 1ps/1ps

module show_state_control(
    input clk,
    input setting_time,
    output reg [2:0] show_state = 3'd0
);
reg [2:0] show_state_tmp;
always@(posedge clk)begin
    if(setting_time == 1'b1)begin
        show_state <= show_state_tmp;
    end
    else begin
        show_state <= show_state + 3'd0;
    end
end

always@(*)begin
    if(show_state < 3'd4)begin
        show_state_tmp = show_state + 3'd1;
    end
    else begin
        show_state_tmp = 3'd0;
    end
end

endmodule