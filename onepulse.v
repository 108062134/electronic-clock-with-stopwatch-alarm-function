`timescale 1ps/1ps

module onepulse(
    input clk,
    input signal,
    output reg one_pulse
);
reg signal_delay;
reg one_pulse_nxt;

always@*
    one_pulse_nxt = signal&(~signal_delay);

always@(posedge clk)begin
    signal_delay <= signal;
end

always@(posedge clk)begin
    one_pulse <= one_pulse_nxt;
end


endmodule