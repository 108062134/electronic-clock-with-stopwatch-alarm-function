`timescale 1ps/1ps

module debounce(
    input clk,
    input signal,
    output reg debounce_signal
);
reg debounce_next ;
reg [3:0] tmp = 4'd0;
always@(posedge clk)begin
    tmp <= {tmp[2:0],signal};
    debounce_signal <= debounce_next;
end
always@(*)begin
    if(tmp == 4'b1111)begin
        debounce_next = 1'b1;
    end
    else begin
        debounce_next = 1'b0;
    end
end


endmodule