`timescale 1ps/1ps

// record how many days for the month
module days_a_month(
    input [3:0] month_tens,
    input [3:0] month_digits,
    output reg[3:0] max_day_a_month_ten,
    output reg[3:0] max_day_a_month_digit
);

always@(*)begin
    if(month_tens == 4'd0 && month_digits == 4'd1)begin
        max_day_a_month_ten = 4'd3;
        max_day_a_month_digit = 4'd1;
    end
    else if(month_tens == 4'd0 && month_digits == 4'd2)begin
        max_day_a_month_ten = 4'd2;
        max_day_a_month_digit = 4'd8;
    end
    else if(month_tens == 4'd0 && month_digits == 4'd3)begin
        max_day_a_month_ten = 4'd3;
        max_day_a_month_digit = 4'd1;
    end
    else if(month_tens == 4'd0 && month_digits == 4'd4)begin
        max_day_a_month_ten = 4'd3;
        max_day_a_month_digit = 4'd0;
    end
    else if(month_tens == 4'd0 && month_digits == 4'd5)begin
        max_day_a_month_ten = 4'd3;
        max_day_a_month_digit = 4'd1;
    end
    else if(month_tens == 4'd0 && month_digits == 4'd6)begin
        max_day_a_month_ten = 4'd3;
        max_day_a_month_digit = 4'd0;
    end
    else if(month_tens == 4'd0 && month_digits == 4'd7)begin
        max_day_a_month_ten = 4'd3;
        max_day_a_month_digit = 4'd1;
    end
    else if(month_tens == 4'd0 && month_digits == 4'd8)begin
        max_day_a_month_ten = 4'd3;
        max_day_a_month_digit = 4'd1;
    end
    else if(month_tens == 4'd0 && month_digits == 4'd9)begin
        max_day_a_month_ten = 4'd3;
        max_day_a_month_digit = 4'd0;
    end
    else if(month_tens == 4'd1 && month_digits == 4'd0)begin
        max_day_a_month_ten = 4'd3;
        max_day_a_month_digit = 4'd1;
    end
    else if(month_tens == 4'd1 && month_digits == 4'd1)begin
        max_day_a_month_ten = 4'd3;
        max_day_a_month_digit = 4'd0;
    end
    else if(month_tens == 4'd1 && month_digits == 4'd2)begin
        max_day_a_month_ten = 4'd3;
        max_day_a_month_digit = 4'd1;
    end
    else begin
        max_day_a_month_ten = 4'd3;
        max_day_a_month_digit = 4'd0;
    end
end


endmodule
