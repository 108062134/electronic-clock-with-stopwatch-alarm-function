`timescale 1ps/1ps
module seven_segment(
    input [4:0] num,
    output reg [6:0] display
);
always@(*)begin
    case(num)   
    5'd0: display = 7'b0000001;
    5'd1: display = 7'b1001111;
    5'd2: display = 7'b0010010;
    5'd3: display = 7'b0000110;
    5'd4: display = 7'b1001100;
    5'd5: display = 7'b0100100;
    5'd6: display = 7'b0100000;
    5'd7: display = 7'b0001111;
    5'd8: display = 7'b0000000;
    5'd9: display = 7'b0000100;
    default:display = 7'b0000001;
    endcase
end


endmodule


